%%%using http://swish.swi-prolog.org/;
%%% - pomocne funkcie
%%% ** %% - konkretne zadanie v prologu na skuske (r. 2017), v zadani nieje povolene pouzivat vstavane funkcie prologu ako member, append atp.


%%% zisti ci je clen P prvkom zoznamu S
jePrvek(P, [P|_]).                              %if P je hlava zoznamu, return true
jePrvek(P, [_|T]) :- jePrvek(P, T).             %else zavolaj nad zvyskom zoznamu

%%% ci je S zoznam
jeSeznam([]).                                   %true ake je zoznam prazdny
jeSeznam([_|_]).                                %true ak ma zoznam strukturu [_|_]

%%% ** %% je seznam S mnozina? (overenie ze nema duplicitni prvky)
jeMnozina([]).                                  %true ak je prazdny
jeMnozina([_]).                                 %true ak je jednoprvkovy
jeMnozina([H|T]) :- jePrvek(H, T), !, fail.     %false a return(! - znamena ze na jeMnozina() riadky za tymto sa nikdy nedostane) ak v zbytku zoznamu T sa nachadza jeho terajsia hlava H (== duplicita)
jeMnozina([_|T]) :- jeMnozina(T).               %ak neskocil do predosleho riadku, pokracuje nad zbytkom zoznamu

%%% ** %% previest seznam S na mnozinu M (odstraneni duplicitnich prvku)
preved([], []).
preved([H|T], M) :- jePrvek(H, T), preved(T, M).    %ak hlava H seznamu S je aj v zbytku S, prevadzame zbytek T seznamu S
preved([H|T], [H|M]) :- preved(T, M).               %inac sa dostavame tu a vkladame hlavu H seznamu S do mnoziny M a pokracujeme zbytkem T seznamu S

%%% najde max ciselny prvok P zoznamu S, maximum(S, P)
maximum([], false).											%v prazdnom nemame maximum
maximum([P], P).											%v jednoprvkovom je to dany prvok / resp. ak nam ostane jeden prvok tak je to on
maximum([H1,H2|T], P) :- H1 >= H2, maximum([H1|T], P).		%mame 2 zaciatocne prvky, H1, H2 ... ak H1 >= H2, vezmeme H1 a zvysok zoznamu a znova porovnavame, H1 je zatial lokalne max, H2 ide "akoby prec"
maximum([H1,H2|T], P):- H1 < H2, maximum([H2|T], P).		%ak je H2 > H1, berieme H2 (lokalne max) a zvysok zoznamu a porovnavame, H1 akoby ide prec, uz ho neberieme v zozname do uvahy...

%%% najde min ciselny prvok, ako maximum, len obratene znamienka
minimum([], false).
minimum([P], P).
minimum([H1,H2|T], P) :- H1 =< H2, minimum([H1|T], P).
minimum([H1,H2|T], P) :- H1 > H2, minimum([H2|T], P).

%%% ** %% najde ciselny index maxima v zozname, potrebujeme na to funkciu maximum() - vyssie, vracia I ako index.
indexMax([], false).											%v prazdnom zozname nemame maximum cize ani jeho index
indexMax([H|T], I) :- maximum([H|T], P), H = P, I is 1, !.		%ak mame zoznam s hlavou H a zbytkom T, tak chceme zistit ci H (prvy prvok) je maximum daneho zoznamu, ak je, vratime index 1 a nepokracujeme dalej (!)
indexMax([_|T], I) :- indexMax(T, I1), I is I1 + 1.				%inac volame indexMax nad zbytkom zoznamu T a pomocnym indexom I1, vysledny index I bude I1+1

%%% ** %% najde index minima, ako indexMax, len sa pouziva operacia minimum() - tiez je vyssie
indexMin([], false).
indexMin([H|T], I) :- minimum([H|T], P), H = P, I is 1, !.
indexMin([_|T], I) :- indexMin(T,I1), I is I1 + 1. 

%%% ** %% max ciselny prvok obecneho zoznamu (obsahuje vnorene zoznamy) 
maximum([],false).
maximum([P],P).
maximum([H1,H2|T],P):-jeSeznam(H1), maximum(H1, P1), maximum([P1,H2|T],P).
maximum([H1,H2|T],P):-jeSeznam(H2), maximum(H2, P1), maximum([H1,P1|T],P).
maximum([H1,H2|T],P):-H1>=H2, maximum([H1|T],P).
maximum([H1,H2|T],P):-H1<H2, maximum([H2|T],P).

%%% prienik R1 a R2 je R3 (obsahuje iba spolocne elementy)
prunik([],_,[]).																%prazdna mnozina ^ hocico = prazdna mnozina
prunik([H1|T1], R2, R3) :- jePrvek(H1, R2), R3 = [H1|T3], prunik(T1, R2, T3).	%ak prvy prvok R1 je aj clenom R2, zapis ho do ich prieniku R3, pokracuj so zvyskom R1
prunik([_|T1], R2, R3):- prunik(T1, R2, R3).									%inac iba pokracuj so zvyskom zoznamu R1

%%% sjednoceni R1 a R2 (obsahuje prvky oboch, ale spolocne len raz)
sjednoceni([],L,L).																	%sjednoceni prazdnej a nejakej L je nejaka mnozina L
sjednoceni([H1|T1], R2, R3) :- sjednoceni(T1, R2, R31), jePrvek(H1,R31), R3 = R31.	%zjednocujem prvy prvok R1 a mnozinu R2 -> zjednocujem zvysok do pomocnej mnoziny R31 a ak je H1 prvok tej pomocnej zjednotenej tak uz ho nepridavam do R3, vysledkom je zatial R31
sjednoceni([H1|T1], R2, R3) :- sjednoceni(T1, R2, R31), R3 = [H1|R31].				%v pripade ze v zjednoteni prvok este nemame, pridavam ho nazaciatok pomocnej R31 mnoziny a (medzi)vysledok zapisujem do R3

%%% ** %% soucet prvnich 2 a poslednich 2, seznam obsahuje alespon 2 prvky
soucetprvni2([H1,H2|_], X) :- X is H1 + H2.		% sucet prvych dvoch, H1, H2, |_ zvysok zoznamu nas nezaujima, ale musi tam byt uvedeny, inac by to nerozoznalo keby ma zoznam viac ako 2 prvky

soucetposledni2([T1,T2], X) :- X is T1 + T2.	% ukoncenie pre rekurziu, kt. je o riadok nizsie, prechadzame rekurzivne zoznam a ked mame presne 2 prvky v nom, su to posledne 2, scitame ich
soucetposledni2([_|T], X) :- soucetposledni2(T,X).		% prechadzam zoznam rekurzivne kym nemam posledne 2 prvky ktore scitam o riadok vyssie

soucetp2p2(S,X) :- soucetprvni2(S,X1), soucetposledni2(S,X2), X is X1 + X2.		% finalny sucet

%%% smaz(prvek, seznam, vystupny seznam)
smaz(_,[],[]).		% hocico ked zmazeme z prazdneho, vrati nam prazdny
smaz(P,[P|T],T) :- !.	% ak P, ktory chceme zmazat je hlavou zoznamu, vratime zvysok a nepokracujeme dalej (!)
smaz(P,[H|T1],[H|T2]):- smaz(P, T1,T2).		% ak P nieje hlavou zoznamu, hlavu prenasame do vystupneho a prevadzame smaz nad zbytkom zoznamu, aj vo vystupnom len zbytok

%%% ** %% soucet dvou nejvetsich prvku v ciselnem seznamu (bez vnorenych) - neviem ci mame predpokladat aspon 2 prvkovy zoznam
soucet2najv([],0).	% v prazdnom sezn. je to 0
soucet2najv([P],P).		%v jednoprvkovom ??, ale asi bolo v zadani napisane, ze zoznam obsahuje aspon 2 prvky... cize len pre istotu
soucet2najv(S, X) :- maximum(S, MAX1), smaz(MAX1,S,S1), maximum(S1, MAX2), X is MAX1 + MAX2.	% zistim MAX1 sezanmu S, zmazem MAX1 cim vytvorim pomocny seznam S1 a zistim jeho MAX2, scitam MAX1 a MAX2

%%% ** %% Zistit, ci prienik S1 a S2 je podmnozinou S3 //vychadza stale true lebo sa ako prienik 2 mnozin berie aj prazdna mnozina, ktora je podmnozinou kazdej mnoziny....
je_podmnozina(S1,S2,S3) :- prunik(S1,S2, S12), prunik(S12,S3, S123), S12 = S123.	%zistim prienik S1 a S2 = S12, a zistim prienik S12 a S3, oznacim S123. Ak S123 je rovnake ako S12, tak true.

%%% ** %% sucet prveho kladneho a posledneho zaporneho v zozname S
prvekladne([K|_], K) :- K > 0.
prvekladne([_|T], K) :- prvekladne(T,K).

poslednezaporne([],_).
poslednezaporne([Z|T], Z) :- Z < 0, poslednezaporne(T,Z).
poslednezaporne([_|T],Z) :- poslednezaporne(T,Z).

sucetKZ(S,X) :- prvekladne(S,K), poslednezaporne(S,Z), X is K + Z.

%%% ** %% linearnost zoznamu
jelinearny([]).
jelinearny([H|_]) :- jeSeznam(H),!, fail.
jelinearny([_|T]) :- jelinearny(T).